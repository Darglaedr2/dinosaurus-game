﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NumericWater : MonoBehaviour
{
    public TMP_InputField TMP_IF;
  

    void OnTriggerEnter(Collider collide)
    {
        if (collide.gameObject.name == "Player")
        {
            StaticClass.CrossSceneInformation = TMP_IF.text;
            SceneManager.LoadSceneAsync(1);
        }
    }
}
