﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerState
{
    void Enter(Player player);

    void Exit(Player player);

    void Notify(string argument);

    IPlayerState Update(Player player);
}
