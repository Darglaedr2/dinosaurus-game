﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunPlayerState : IPlayerState
{
    public void Enter(Player player) { }

    public void Exit(Player player) { }

    public void Notify(string argument) { }

    public IPlayerState Update(Player player)
    {
        Vector3 direction = new Vector3();
        direction.x = Convert.ToSingle(Input.GetKey(KeyCode.D)) - Convert.ToSingle(Input.GetKey(KeyCode.Q));
        direction.z = Convert.ToSingle(Input.GetKey(KeyCode.Z)) - Convert.ToSingle(Input.GetKey(KeyCode.S));

        if (direction.magnitude != 0f)
        {
            //player.transform.Translate(direction.normalized * player.Speed * Time.deltaTime);

            if (Input.GetKey(KeyCode.Space))
            {
                //player.Jump();
            }
        }
        return new IdlePlayerState();
    }
}
