﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdlePlayerState : IPlayerState
{
    public void Enter(Player player) { }

    public void Exit(Player player) { }

    public void Notify(string argument) { }

    public IPlayerState Update(Player player)
    {
        if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            return new RunPlayerState();
        }
        if (Input.GetKey(KeyCode.Space))
        {
           // player.Jump();
        }
        return null;
    }
}
