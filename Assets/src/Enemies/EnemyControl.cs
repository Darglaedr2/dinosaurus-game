﻿using System;
using System.Collections;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyControl : MonoBehaviour
{
    // Stockage du navmesh (pathfinding) et la distance de déplacement automatique
    private NavMeshAgent navAgent;
    private float wanderDistance = 4;

    public Target player;

    // Variable contenant le fichier de notre ennemi (contenant les caractèristiques : nom, description, modèle 3D, etc.)
    public EnemyData enemyData;

    //States
    public bool playerInSightRange, playerInAttackRange;

    // animation
    public GameObject impactEffect;

    public int damage;


    public float timer = 0;
    public bool timerReached = false;

    // rayon lumineux
    public ParticleSystem muzzleFlash;

    private void Start()
    {
        // S'il y a un NavMeshAgent sur l'empty, on le recupère (toujours appelé avec notre exemple)
        if (navAgent == null)
        {
            navAgent = GetComponent<NavMeshAgent>();
        }

        navAgent.speed = enemyData.speed;
    }

    private void Update()
    {
        // Si enemyData n'est pas assigné, on ne fait rien dans Update
        if (enemyData == null)
        {
            return;
        }

        //Check for sight and attack range
        float distance = Vector3.Distance(transform.position, player.transform.position);
        playerInSightRange = distance <= enemyData.sightRange;
        playerInAttackRange = distance <= enemyData.attackRange;
        //Debug.Log("Distance:  " + distance.ToString("R"));
        if (playerInAttackRange)
        {
           
            timer += Time.deltaTime;
            Debug.Log(enemyData.attackInterval);
            if (timer > enemyData.attackInterval)
            {
                Debug.Log("Done waiting");
                Debug.Log("ATTACK");
                Attack();       
                timer = 0;
            }
            
            
        }
        else
        {
            timer = 0;
        }
        if (!playerInSightRange && !playerInAttackRange) Patroling();
        if (playerInSightRange && !playerInAttackRange) ChasePlayer();
    }

    // Recupère une nouvelle destination a proximité (basé sur la var wanderDistance)
    private void Patroling()
    {
        navAgent.speed = enemyData.speed;
        if (navAgent.remainingDistance < 1f)
        {
            Vector3 nextDestination = transform.position;
            nextDestination += wanderDistance * new Vector3(UnityEngine.Random.Range(-1f, 1f), 0f, UnityEngine.Random.Range(-1f, 1f)).normalized;

            NavMeshHit hit;
            if (NavMesh.SamplePosition(nextDestination, out hit, 3f, NavMesh.AllAreas))
            {
                navAgent.SetDestination(hit.position);
            }
        }
    }

    private void ChasePlayer()
    {
        //Debug.Log("ChasePlayer:  " + player.position.ToString());
        transform.LookAt(player.transform);
        navAgent.speed = enemyData.speed * 2;
        navAgent.SetDestination(player.transform.position);
    }

    private void Attack()
    {
        Vector3 vct = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z +1);
        Debug.Log(player);
        if (player != null)
        {
            player.TakeDamage(damage);
         
        }
        muzzleFlash.Play();
        GameObject impactGO = Instantiate(impactEffect, vct, Quaternion.LookRotation(vct));
        Destroy(impactGO, 1f);
    }

}
