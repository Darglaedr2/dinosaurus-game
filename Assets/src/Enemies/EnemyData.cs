﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "DinoGame/EnemyData")]
public class EnemyData : ScriptableObject
{
    public GameObject enemyModel;
    public int health = 100;
    public int damage = 1;
    public float speed = 1.5f;
    public int sightRange = 15;
    public int attackRange = 10;
    public float attackInterval = 0.5f;
    public Canvas HealthBar;
    public ParticleSystem dinoBeam;
}
