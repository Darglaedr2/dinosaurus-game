﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Target : MonoBehaviour
{
    // Start is called before the first frame update
    public int maxHealth = 100;
    public int currentHealth;
    public List<GameObject> prefabs;
    public HealthBarScript healthBar;

    public TMP_InputField TMP_IF;

    public List<Spawn> spawns;

    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        TMP_IF.interactable = true;
    }

    public void TakeDamage(int amount)
    {
        Debug.Log("dmg: " + amount);
        currentHealth -= amount;
        Debug.Log("dmg: " + currentHealth);
        healthBar.SetHealth(currentHealth);
        if (currentHealth <= 0)
        {
            Debug.Log("DIE ");
            Die();
        }
    }

    void Die()
    {
        if (gameObject.name.Equals("Player")){
            StaticClass.CrossSceneInformation = TMP_IF.text;
            SceneManager.LoadScene(1);
        }

        Destroy(gameObject);
        GameObject newObjectSpawn = Instantiate(prefabs[0]);
        newObjectSpawn.name = "Donut";
        newObjectSpawn.transform.position = new Vector3(transform.position.x,transform.position.y + 1,transform.position.z);
        if(TMP_IF != null)
        {
            int convertedScoreValue = int.Parse(TMP_IF.text);
            convertedScoreValue = convertedScoreValue + 1;
            TMP_IF.text = convertedScoreValue.ToString();
        }
        foreach(Spawn s in spawns)
        {
            s.InvokingDino();
        }
    }

    void OnTriggerEnter(Collider collide)
    {
        if (collide.gameObject.name == "Donut")
        {
            if (currentHealth >= 81)
            {
                currentHealth = 100;
                healthBar.SetHealth(currentHealth);
            }
            else
            {
                currentHealth += 20;
                healthBar.SetHealth(currentHealth);
            }
        }
    }
}
