﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public List<GameObject> prefabs;
    public TMP_InputField TMP_IF;


    // Start is called before the first frame update
    void Start()
    {

        Invoke("SpawnI", 0f);
    }

    public void InvokingDino (){
        Invoke("SpawnI", 0f);
    }
    void SpawnI()
    {
        GameObject newObjectSpawn = Instantiate(prefabs[0],transform.position, transform.rotation);
        if(null != newObjectSpawn.GetComponent<EnemyControl>() && int.Parse(TMP_IF.text) > 3)
        {
            Debug.Log("Increasing Dinosaur speed");
            newObjectSpawn.GetComponent<EnemyControl>().enemyData.speed += int.Parse(TMP_IF.text);
        }
    }
}
